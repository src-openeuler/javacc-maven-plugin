Name:           javacc-maven-plugin
Version:        3.0.1
Release:        1
Summary:        Maven Plugin for processing JavaCC grammar files
License:        ASL 2.0
URL:            https://www.mojohaus.org/
BuildArch:      noarch
Source0:        https://github.com/mojohaus/javacc-maven-plugin/archive/javacc-maven-plugin-%{version}.tar.gz
Source1:        http://www.apache.org/licenses/LICENSE-2.0.txt
BuildRequires:  maven-local mvn(junit:junit) mvn(net.java.dev.javacc:javacc)
BuildRequires:  mvn(org.apache.maven.doxia:doxia-sink-api) mvn(org.apache.maven.doxia:doxia-site-renderer)
BuildRequires:  mvn(org.apache.maven:maven-model) mvn(org.apache.maven:maven-plugin-api)
BuildRequires:  mvn(org.apache.maven:maven-project) mvn(org.apache.maven.reporting:maven-reporting-api)
BuildRequires:  mvn(org.apache.maven.reporting:maven-reporting-impl) mvn(org.codehaus.mojo:mojo-parent:pom:)
BuildRequires:  mvn(org.codehaus.plexus:plexus-utils) mvn(org.apache.maven.plugins:maven-plugin-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-invoker-plugin)

%description
Process JavaCC grammars.

%package     help
Summary:     Help documentation for %{name}
Provides:    %{name}-javadoc = %{version}-%{release}
Obsoletes:   %{name}-javadoc < %{version}-%{release}

%description help
Help documentation for %{name}.

%prep
%autosetup -n %{name}-javacc-maven-plugin-%{version} -p1
cp -p %{SOURCE1} .

%pom_remove_dep edu.ucla.cs.compilers:jtb
rm -fr src/it
rm -fr src/site

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc LICENSE-2.0.txt src/main/resources/NOTICE

%files help -f .mfiles-javadoc
%doc src/main/resources/NOTICE

%changelog
* Thu Jul 27 2023 yaoxin <yao_xin001@hoperun.com> - 3.0.1-1
- Update to 3.0.1

* Tue Mar 3 2020 shijian <shijian16@huawei.com> - 2.6-27
- Package init
